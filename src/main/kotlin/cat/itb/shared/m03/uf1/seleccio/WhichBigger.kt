package cat.itb.shared.m03.uf1.seleccio

import java.util.*

fun main() {
    val lector = Scanner(System.`in`)
    val firstNumber: Int = lector.nextInt()
    val secondNumber: Int = lector.nextInt()

    if (firstNumber > secondNumber) {
        println(firstNumber)
    } else {
        println(secondNumber)
    }

    /*
    val max = if (firstNumber > secondNumber) firstNumber else secondNumber
    println(max)
    */

}
